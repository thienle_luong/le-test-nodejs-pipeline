// Load the SDK and UUID
const AWS = require('aws-sdk');

// Create an S3 client
const s3EcsClient = new AWS.ECS({region: "us-east-1"});
const s3EcrClient = new AWS.ECR({region: "us-east-1"});


// s3EcsClient.listClusters((err, {clusterArns}) => {
//     if (err) console.log(err)
//     else {

//         clusterArns.forEach(cluster => {
//             s3EcsClient.listTasks({cluster: cluster}, (err, {taskArns}) => {
//                 if (err) console.log(err)
//                 else {
//                     if (taskArns.length > 0) {
//                         s3EcsClient.describeTasks({tasks: taskArns, cluster: cluster}, (err, data) => {
//                             if (err) console.log(err)
//                             else {
//                                 console.log(data)
//                             }
//                         })

//                     }
//                 }
//             })
//         })
//     }
// })
s3EcrClient.describeRepositories({}, (err, {repositories}) => {
    if (err) console.log(err, err.stack); // an error occurred
    else {
        repositories.forEach(repo => {
            s3EcrClient.describeImages({
                repositoryName: repo.repositoryName
            }, (err, data) => {
                if (err) console.log(err)
                else {
                    console.log(data)
                }
            })
        })
    } 
})